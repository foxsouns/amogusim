=begin
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
=end

# main vars
players = 15
imptot = 3
crewtot = players - imptot
tasks = crewtot * 4

impostors = imptot # impostors, dynamic
crewmates = crewtot # crewmates, dynamic

# some variables i use in the program
tasks_done = 0 
avar = 0
bvar = 0

puts 
puts "there are " + crewmates.to_s + " crewmates, and " + impostors.to_s + " impostors.\n" 
  
  
# while the game is not beaten; when there is still imps, crew outnumbers imps, and there is no taskwin
while impostors > 0 and crewmates > impostors and tasks > tasks_done
  # emulate tasks
    tasks_done = tasks_done + rand(crewtot + 1) # randomly decide how many tasks to finish per iteration, based off of crewmates
    if tasks_done > tasks; tasks_done = tasks; end # make sure we never have more tasks done than we have tasks total
    puts "tasks done: " + ((tasks_done.to_f / tasks)*100).to_i.to_s + "%" 
   
  # calculate kills, if any
    avar = impostors
    while avar > 0 # avar here says how many times to run the code
      avar = avar - 1 # every time we run the code, decrease avar
      bvar = bvar + rand(4) - 1 # bvar is the kills for this turn. randomly chosen from 0, 1, and 2 per imp, with more weight on lower hits to keep it fair
      if bvar < 0; bvar = 0; end # however, that weight can sometimes end up with a reverse of already killed people! need to make sure that doesnt happen
    end
    unless bvar == 0 # unless we aint killin nobody
      crewmates = crewmates - bvar # kill crewmates
      puts bvar.to_s + " crewmates died last round :(" 
      bvar = 0 # reset bvar so i can use it in the next round
    else
      puts "no crewmates died last round! :>" 
    end
    if impostors > 0 and crewmates > impostors and tasks > tasks_done # this is a check to make sure we dont throw out another person while the game should have ended.
      avar = rand(crewmates+impostors+(crewmates+impostors)*0.25) # num from 0-10
      if avar <= impostors # chunk an impostor
        impostors = impostors - 1
        puts "an impostor has been thrown out! >:3c" 
      elsif avar <= crewmates+impostors # chunk a crew
          if rand(4) == 0
            crewmates = crewmates - 1
            puts "a crewmate has been thrown out! >:(" 
          else
            puts "nobody was thrown out." 
          end
        else # skip
        puts "nobody was thrown out." 
      end
    end
    if crewmates < 0; crewmates = 0; end # make sure we dont end up with negative crewmates
    puts
    puts "there are " + crewmates.to_s + " crewmates, and " + impostors.to_s + " impostors." 
    puts
end
if crewmates > impostors # if, after the game is over, there are still more crew than imps, crew wins! but we still need to figure out how.
  puts "crewmates win! pog" 
else # anything else has to be an imp win
  puts "impostors win! sadge" 
end
