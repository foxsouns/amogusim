# AmoguSim

a pretty sus among us simulator i made one day. (Un)licensed with The Unlicense.

## How do I run this!!??!

### linux
install ruby. its probably in your package manager as `ruby`, i know for sure this is true on debian and arch. after you have ruby, run in your terminal:
```
git clone https://gitlab.com/foxsouns/amogusim.git
cd amogusim
ruby amogusim.rb
```

### windows
first, install ruby. i am not a windows user, but [RubyInstaller](https://rubyinstaller.org/) seems like it will work. then, copy the contents of the files into wherever you code on ri, and run it. :shrug:
